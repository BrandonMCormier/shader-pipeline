// Input:
//   N  3D unit normal vector
// Outputs:
//   T  3D unit tangent vector
//   B  3D unit bitangent vector
void tangent(in vec3 N, out vec3 T, out vec3 B)
{
  /////////////////////////////////////////////////////////////////////////////
  // Replace with your code 
  //polar axis, tangent between polar axis and point vector will always be tangent in a unit circle
  vec3 A = vec3(0,0,1);
  T = normalize(cross(A,N));
  B = normalize(cross(N,T));
  /////////////////////////////////////////////////////////////////////////////
}
