//https://flafla2.github.io/2014/08/09/perlinnoise.html modified octave function
float OctavePerlin(vec3 s,float frequency,float amplitude, int octaves, float persistence) {
    float total = 0;
    float maxValue = 0;  // Used for normalizing result to 0.0 - 1.0
    for(int i=0;i<octaves;i++) {
        vec3 noise = vec3(s.x*frequency, s.y*frequency, s.z*frequency);
        total += improved_perlin_noise(noise) * amplitude;
        
        maxValue += amplitude;
        
        amplitude *= persistence;
        frequency *= 2;
    }
    
    return total/amplitude;
}

// Create a bumpy surface by using procedural noise to generate a height (
// displacement in normal direction).
//
// Inputs:
//   is_moon  whether we're looking at the moon or centre planet
//   s  3D position of seed for noise generation
// Returns elevation adjust along normal (values between -0.1 and 0.1 are
//   reasonable.
float bump_height( bool is_moon, vec3 s)
{
  /////////////////////////////////////////////////////////////////////////////
  // Replace with your code 
  //keeping it within acceptable limits, but it does go a little outside of the -0.1-0.1 bounds. 
  //The planet looked nice though and I liked using the octave function, the final product in planet.fs
  //is really textured
  if (is_moon) return OctavePerlin(s,1,1,6,0.5)/50;
  else return OctavePerlin(s,1,1,6,0.5)/50;
  /////////////////////////////////////////////////////////////////////////////
}
