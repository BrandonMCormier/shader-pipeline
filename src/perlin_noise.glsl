// Given a 3d position as a seed, compute a smooth procedural noise
// value: "Perlin Noise", also known as "Gradient noise".
//
// Inputs:
//   st  3D seed
// Returns a smooth value between (-1,1)
//
// expects: random_direction, smooth_step
float perlin_noise( vec3 st) 
{
  /////////////////////////////////////////////////////////////////////////////
  // Replace with your code 
  //perlin noise from wikipedia 
  //https://en.wikipedia.org/wiki/Perlin_noise
  //https://www.scratchapixel.com/lessons/procedural-generation-virtual-worlds/perlin-noise-part-2

  

  //grid cell coordinates
  vec3 grid1 = floor(st) + vec3(0,0,0);
  vec3 grid2 = floor(st) + vec3(1,0,0);
  vec3 grid3 = floor(st) + vec3(0,1,0);
  vec3 grid4 = floor(st) + vec3(1,1,0);
  vec3 grid5 = floor(st) + vec3(0,0,1);
  vec3 grid6 = floor(st) + vec3(1,0,1);
  vec3 grid7 = floor(st) + vec3(0,1,1);
  vec3 grid8 = floor(st) + vec3(1,1,1);

  //gradients
  vec3 grad1 = random_direction(grid1);
  vec3 grad2 = random_direction(grid2);
  vec3 grad3 = random_direction(grid3);
  vec3 grad4 = random_direction(grid4);
  vec3 grad5 = random_direction(grid5);
  vec3 grad6 = random_direction(grid6);
  vec3 grad7 = random_direction(grid7);
  vec3 grad8 = random_direction(grid8);

  //dot products
  float dot1 = dot(grad1, fract(st) - vec3(0,0,0));
  float dot2 = dot(grad2, fract(st) - vec3(1,0,0));
  float dot3 = dot(grad3, fract(st) - vec3(0,1,0));
  float dot4 = dot(grad4, fract(st) - vec3(1,1,0));
  float dot5 = dot(grad5, fract(st) - vec3(0,0,1));
  float dot6 = dot(grad6, fract(st) - vec3(1,0,1));
  float dot7 = dot(grad7, fract(st) - vec3(0,1,1));
  float dot8 = dot(grad8, fract(st) - vec3(1,1,1));


  vec3 smoothed = smooth_step(fract(st));

  //interpolation
  float x1 = mix(dot1, dot2, smoothed.x);
  float x2 = mix(dot3, dot4, smoothed.x);
  float x3 = mix(dot5, dot6, smoothed.x);
  float x4 = mix(dot7, dot8, smoothed.x);

  float y1 = mix(x1,x2,smoothed.y);
  float y2 = mix(x3,x4,smoothed.y);

  return mix(y1,y2,smoothed.z);
  /////////////////////////////////////////////////////////////////////////////
}

